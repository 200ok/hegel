#!/usr/bin/env ruby

require 'active_support/core_ext/hash/conversions'
require 'pp'

# Referenznummer (kann mehrfach auftauchen)
# BkToCstmrDbtCdtNtfctn.Ntfctn.Ntry.NtryDtls
#  .TxDtls.RmtInf.Strd.CdtrRefInf.ref
# NtryRef

module CamtToV11

  extend self

  def convert(data, opts)
    doc = Hash.from_xml(data)
    entries = doc['Document']['BkToCstmrDbtCdtNtfctn']['Ntfctn']['Ntry']
    result = []

    #edetails = nil
    #edetail = nil

    entries.each do |entry|
      entry_ref = entry['NtryRef']
      book_date = entry['BookgDt']['Dt'].tr_s('-', '').slice(2,6)
      value_date = entry['ValDt']['Dt'].tr_s('-', '').slice(2,6)
      details = entry['NtryDtls']['TxDtls']
      details = [details] if details.is_a?(Hash)
      #edetails = details
      details.each do |detail|
        #edetail = detail
        #puts edetail
        tp = detail['Refs']['Prtry']['Tp']
        ref = detail['RmtInf']['Strd']['CdtrRefInf']['Ref']
        puts '-'*80
        pp detail
        if opts[:iban]
          related_parties = detail['RltdPties']
          debitor_account = related_parties && related_parties['DbtrAcct']
          iban = debitor_account && debitor_account['Id']['IBAN']
        end
        amount = (detail['Amt'].to_f * 100).to_i
        tmp = detail['Refs']['Prtry']['Ref']
        dat1 = tmp.slice(2,6)
        ref2_1 = tmp.slice(8,4)
        ref2_2 = tmp.slice(12,4)
        ref2 = [ref2_1, ref2_2] * '  '
        if tp == '01'
          dat1 = detail['RltdDts']['AccptncDtTm'].tr_s('-', '').slice(2,6)
          ref2 = '0000  0000'
        end
        reject_code = detail['RmtInf']['Strd']['AddtlRmtInf'].slice(-1)

        cost = 0
        if charges = detail['Chrgs']
          cost = (charges['TtlChrgsAndTaxAmt'].to_f * 100).to_i
        end

        rline = "%1s%2s%9s%27s%010d%10s%6s%6s%6s%9s%1s%9s%04d" %
          [0,
           tp,
           entry_ref,
           ref,
           amount,
           ref2,
           dat1,
           book_date,
           value_date,
           '0' * 9,
           reject_code,
           '0' * 9,
           cost]

        rline += " #{iban}" if opts[:iban]
        result << rline
      end
    end

    result * "\n"

  #rescue => e
  #  puts e.message
  #  puts e.backtrace
  #  puts
  #  puts 'DETAILS'
  #  puts
  #  pp edetails
  #  puts
  #  puts 'DETAIL'
  #  puts
  #  puts edetail.inspect
  #  exit
  end
end

if $0 == __FILE__
  file = ARGV.first
  xml = open(file).read
  puts CamtToV11.convert(xml, iban: true)
end
