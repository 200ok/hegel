require 'sinatra'

require './camt_to_v11'

mime_type :v11, 'application/octet-stream'

  # set :port, ENV['PORT'] || 9292
# puts settings.port

configure :production do
  # Configure stuff here you'll want to
  # only be run at Heroku at boot

  # TIP:  You can get you database information
  #       from ENV['DATABASE_URI'] (see /env route below)
end

# get '/env' do
#   ENV.inspect
# end

get "/" do
  erb :form
end

post '/process' do
  filename = params[:file][:filename]
  file = params[:file][:tempfile]
  iban = params[:iban]
  content = file.read

  attachment filename + '.v11'
  CamtToV11.convert(content, iban: iban)
end
