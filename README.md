Welcome to Hegel
================

Hegel is not only one of the most influential philosophers of
post-Kantian idealism, but also a swiss german word for a pocket
knive, and most recently also a tool to convert camt to v11.

Usage
-----

It comes with a web interface which is deployed at

> http://hegel.herokuapp.com

But if you're fine with running a Ruby script locally you probably
just want to...

    ./camt_to_v11.rb file.camt > file.v11


That's all folks.
